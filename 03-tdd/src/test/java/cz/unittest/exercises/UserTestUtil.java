package cz.unittest.exercises;

import org.joda.time.LocalDate;

public class UserTestUtil {

  public static User createUser(LocalDate dateOfBirth) {
    return new User("Test user", dateOfBirth);
  }
}
