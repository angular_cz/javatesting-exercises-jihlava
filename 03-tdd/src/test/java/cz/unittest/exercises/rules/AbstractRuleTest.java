package cz.unittest.exercises.rules;

import cz.unittest.exercises.User;
import cz.unittest.exercises.UserTestUtil;
import org.joda.time.LocalDate;
import org.junit.Before;

public class AbstractRuleTest {

  protected CalculationRule rule;

  protected User retired;
  protected User child;
  protected User young;
  protected User adult;

  @Before
  public void prepareUsers() throws Exception {
    retired = UserTestUtil.createUser(new LocalDate(1930, 1, 1));
    child = UserTestUtil.createUser(new LocalDate(2010, 1, 1));
    young = UserTestUtil.createUser(new LocalDate(2000, 1, 1));
    adult = UserTestUtil.createUser(new LocalDate(1980, 1, 1));
  }
}
