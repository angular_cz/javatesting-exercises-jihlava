package cz.unittest.exercises.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ListPage{

  @FindBy(tagName = "h2")
  WebElement titleH2;

  @FindBy(css = "div.beer-list > table")
  WebElement table;

  private final WebDriverWait wait;
  private WebDriver driver;

  public ListPage(WebDriver driver) {
    PageFactory.initElements(driver, this);

    this.driver = driver;
    wait = new WebDriverWait(driver, 10);
    wait.until(ExpectedConditions.visibilityOf(table));
  }

  public String getTitle() {
    return titleH2.getText();
  }

  public void setDegree(String degree) {

    WebElement degreeElement = table.findElement(By.cssSelector("beer-degree-select > select"));
    Select select = new Select(degreeElement);
    select.selectByVisibleText(degree);
  }

  public void setQuery(String query) {
    WebElement queryElement = table.findElement(By.cssSelector("input[ng-model=\"filterController.filter.query\"]"));
    queryElement.clear();
    queryElement.sendKeys(query);
  }

  public ListItem getRowContaining(String name) {
    wait.until(
      ExpectedConditions.textToBePresentInElementLocated(
        By.cssSelector("div.beer-list > table > tbody > tr"), name));

    List<WebElement> rows = table.findElements(By.cssSelector("div.beer-list > table > tbody > tr"));

    for (WebElement row : rows) {

      ListItem listItem = new ListItem(row, driver);
      if (listItem.getName().equals(name)) {
        return listItem;
      }
    }

    return null;
  }
}
