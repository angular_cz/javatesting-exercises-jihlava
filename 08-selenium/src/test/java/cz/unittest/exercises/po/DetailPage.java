package cz.unittest.exercises.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DetailPage {

  private final WebElement detail;
  private WebDriver driver;

  public DetailPage(WebDriver driver) {
    this.driver = driver;
    WebDriverWait wait = new WebDriverWait(driver, 10);
    detail = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.detail")));
  }

  public String getTitle() {
    return detail.findElement(By.tagName("h2")).getText();
  }

  // TODO 3.1 Vytvořte metodu pro přechod na hodnocení

}
