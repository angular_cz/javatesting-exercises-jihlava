package cz.unittest.exercises;

import org.assertj.core.api.Condition;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

// TODO - 2.3.1 Import vlastního assertThat
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AdvancedAssertJMatchersTest {

  public static final Publication FAVORITE_BOOK = new Publication("ID001CZ", PublicationType.BOOK, "Guide to galaxy", 352);
  public static final User PETR = new User("Petr Novák");
  public static final User PAVEL = new User("Pavel Novák");

  public static final List<Publication> SELECTED_ITEMS = Arrays.asList(
    new Publication("ID001CZ", PublicationType.BOOK, "Guide to galaxy", 352, PETR),
    new Publication("ID002SK", PublicationType.BOOK, "Goodbye and thank you for fishes", 630, PETR),
    new Publication("ID003SK", PublicationType.MAGAZINE, "Ikarie", 52, PAVEL)
  );

  /**
   * TODO 2.1 Test základních kritérií
   */
  @Test
  public void favoriteBookMeetsBasicCriteria() throws Exception {
    assertEquals(PublicationType.BOOK, FAVORITE_BOOK.getType());
    assertTrue(FAVORITE_BOOK.getPages() > 200);

    assertTrue(FAVORITE_BOOK.getId().startsWith("ID"));
    assertFalse(FAVORITE_BOOK.getId().endsWith("EN"));
  }

  /**
   * TODO - 2.2 Test vlastnosti objektu
   */
  @Test
  public void objectCanBeAssertedByPropertyWithValue() {
    Publication bookWithExpectedValues = new Publication("ID001CZ", PublicationType.BOOK, "Guide to galaxy", 352);

    assertEquals(bookWithExpectedValues.getId(), FAVORITE_BOOK.getId());
    assertEquals(bookWithExpectedValues.getType(), FAVORITE_BOOK.getType());
    assertEquals(bookWithExpectedValues.getName(), FAVORITE_BOOK.getName());
    assertEquals(bookWithExpectedValues.getPages(), FAVORITE_BOOK.getPages());
  }

  /**
   * TODO - 2.3.2 Použití vlastní assertThat
   */
  @Test
  public void objectCanBeAssertedByOwnAssertThatMethod() throws Exception {
    assertEquals(PublicationType.BOOK, FAVORITE_BOOK.getType());
  }

  /**
   * TODO - 2.4.1 Test ověření vlastnosti pomocí condition
   */
  @Test
  public void objectCanBeAssertedByCondition() {
    assertEquals(PublicationType.BOOK, FAVORITE_BOOK.getType());
  }

  public Condition<? super Publication> type(final PublicationType type) {
    return new Condition<Publication>() {
      @Override
      public boolean matches(Publication publication) {

        // TODO - 2.4.2 Ověřte typ publikace
        return false;
      }
    };
  }

  /**
   * TODO - 2.5 Test neexistence objektu v kolekci
   */
  @Test
  public void thereIsNoPosterAmongSelectedItems() {

    boolean thereIsPoster = false;
    for (Publication publication : SELECTED_ITEMS) {
      if (publication.getType() == PublicationType.POSTER) {
        thereIsPoster = true;
        break;
      }
    }

    assertFalse(thereIsPoster);
  }

  /**
   * TODO - 2.6 Test vlastností podmnožiny objektů
   */
  @Test
  public void allBooksFromSelectedItemsShouldBeReservedForPetr() {

    for (Publication publication : SELECTED_ITEMS) {
      if (publication.getType() == PublicationType.BOOK) {
        assertEquals(PETR, publication.getReservedFor());
      }
    }
  }
}
