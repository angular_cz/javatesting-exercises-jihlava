package cz.unittest.exercises.assertions;

import cz.unittest.exercises.Publication;

/**
 * Entry point for assertions of different data types. Each method in this class is a static factory for the
 * type-specific assertion objects.
 */
public class Assertions extends org.assertj.core.api.Assertions{

  /**
   * Creates a new instance of <code>{@link PublicationAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  public static PublicationAssert assertThat(Publication actual) {
    return new PublicationAssert(actual);
  }

  /**
   * Creates a new <code>{@link Assertions}</code>.
   */
  protected Assertions() {
    // empty
  }
}
