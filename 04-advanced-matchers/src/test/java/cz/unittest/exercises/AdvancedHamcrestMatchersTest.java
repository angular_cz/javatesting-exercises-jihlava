package cz.unittest.exercises;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class AdvancedHamcrestMatchersTest {

  public static final Publication FAVORITE_BOOK = new Publication("ID001CZ", PublicationType.BOOK, "Guide to galaxy", 352);
  public static final List<Publication> SELECTED_BOOKS = Arrays.asList(
    new Publication("ID001CZ", PublicationType.BOOK, "Guide to galaxy", 352),
    new Publication("ID002SK", PublicationType.BOOK, "Goodbye and thank you for fishes", 630),
    new Publication("ID003EN", PublicationType.BOOK, "Clean code", 520)
  );
  public static final List<String> SELECTED_BOOK_IDS = Arrays.asList("ID001CZ", "ID002SK", "ID003EN");

  /**
   * TODO - 1.1 Test základních kritérií
   */
  @Test
  public void favoriteBookMeetsBasicCriteria() throws Exception {
    assertEquals(PublicationType.BOOK, FAVORITE_BOOK.getType());
    assertTrue(FAVORITE_BOOK.getPages() > 200);

    assertTrue(FAVORITE_BOOK.getId().startsWith("ID"));
    assertFalse(FAVORITE_BOOK.getId().endsWith("EN"));
  }

  /**
   * TODO - 1.2 Test všech záznamů v kolekci
   */
  @Test
  public void allSelectedBookIdsStartsWithID() throws Exception {

    for (String bookId : SELECTED_BOOK_IDS) {
      assertTrue(bookId.startsWith("ID"));
    }
  }

  /**
   * TODO - 1.3 Test existence záznamu
   */
  @Test
  public void atLeastOnSelectedBookEndsWithSK() throws Exception {

    boolean hasItemWithSK = false;
    for (String bookId : SELECTED_BOOK_IDS) {
      if (bookId.endsWith("SK")) {
        hasItemWithSK = true;
        break;
      }
    }

    assertTrue(hasItemWithSK);
  }

  /**
   * TODO - 1.4 Test vlastnosti objektu
   */
  @Test
  public void objectPropertyCanBeTestedWithHasPropertyMatcher() throws Exception {
    assertEquals(PublicationType.BOOK, FAVORITE_BOOK.getType());
  }

  /**
   * TODO - 1.5.1 Test použití vlastního matcheru
   */
  @Test
  public void objectPropertyCanBeTestedWithOwnMatcher() throws Exception {
    assertEquals(PublicationType.BOOK, FAVORITE_BOOK.getType());
  }

  private Matcher<Publication> isType(final PublicationType type) {
    return new TypeSafeMatcher<Publication>() {

      @Override
      protected boolean matchesSafely(Publication publication) {

        // TODO - 1.5.2 Ověřte typ publikace
        return false;
      }

      public void describeTo(Description description) {
        description.appendText("publication with type")
          .appendValue(type);
      }
    };
  }

  /**
   * TODO - 1.6 Test vlastností objektů v kolekci
   */
  @Test
  public void selectedBooksMeetsSchoolCriteria() throws Exception {

    for (Publication publication : SELECTED_BOOKS) {
      assertEquals(PublicationType.BOOK, publication.getType());
    }
  }
}
