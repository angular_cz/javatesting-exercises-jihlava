package cz.unittest.exercises;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/cz/unittest/exercises/features")
public class CucumberFeaturesTest {

}
