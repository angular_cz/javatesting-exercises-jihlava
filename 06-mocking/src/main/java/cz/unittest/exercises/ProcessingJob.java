package cz.unittest.exercises;

import cz.unittest.exercises.domain.Borrowing;
import cz.unittest.exercises.domain.User;
import cz.unittest.exercises.reminder.ReminderContent;
import cz.unittest.exercises.reminder.ReminderQueue;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class ProcessingJob {

  private BorrowingsRepository borrowingsRepository;
  private ReminderQueue reminderQueue;

  public ProcessingJob(BorrowingsRepository borrowingsRepository, ReminderQueue reminderQueue) {
    this.borrowingsRepository = borrowingsRepository;
    this.reminderQueue = reminderQueue;
  }

  // TODO - 3.2 Rozšiřte implementaci jobu o podporu seskupování
  public int process() {
    List<Borrowing> borrowings = borrowingsRepository.getOverdueBorrowings();

    for (Borrowing borrowing : borrowings) {
      reminderQueue.add(borrowing.getUser(), new ReminderContent(borrowing));
    }

    return borrowings.size();
  }

}
