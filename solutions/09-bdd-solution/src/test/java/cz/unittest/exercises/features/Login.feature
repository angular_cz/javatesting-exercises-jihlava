Feature: User can login

  Scenario: Logging user
    Given homepage
    And user shows login dialog
    When log with credentials "user" / "pass"
    Then user is logged in as "user"
