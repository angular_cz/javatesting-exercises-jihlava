Feature: Logged user can rate beer

  Background:
    Given homepage
    And user shows login dialog
    And log with credentials "user" / "pass"

  Scenario: Logged user can rate beer
    Given beer detail with id 2
    When user clicks on rating tab
    And writes name "Petr"
    And selects 5 stars
    And writes description
    And click add rating
    Then rating is successfully created

  Scenario: Logged user can rate beer - simplified
    Given beer detail with id 2
    When user clicks on rating tab
    And fill rating
    And click add rating
    Then rating is successfully created

