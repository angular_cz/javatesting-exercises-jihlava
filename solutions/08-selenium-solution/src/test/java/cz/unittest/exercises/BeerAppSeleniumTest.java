package cz.unittest.exercises;

import cz.unittest.exercises.po.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class BeerAppSeleniumTest {

  public static final String BASE_URI = "http://beer-app.angular.cz/";

  @Rule
  public WebDriverRule webDriverRule = new WebDriverRule();
  private WebDriver driver;

  @Before
  public void setUp() throws Exception {
    driver = webDriverRule.getDriver();
    driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

    driver.get(BASE_URI);
  }

  @Test
  public void testHomepageTitle() throws InterruptedException {
    ListPage listPage = new ListPage(driver);
    assertThat(listPage.getTitle()).isEqualTo("Seznam piv");
  }

  @Test
  public void searchRebel11AndGoToTheDetail() throws InterruptedException {

    ListPage listPage = new ListPage(driver);
    listPage.setDegree("11");
    listPage.setQuery("Rebel");

    ListItem rowWithRebel = listPage.getRowContaining("Rebel");
    assertThat(rowWithRebel.getName()).isEqualTo("Rebel");

    DetailPage detailPage = rowWithRebel.goToDetail();
    assertThat(detailPage.getTitle()).isEqualTo("Rebel");
  }

  @Test
  public void ratingBeerWithExplicitLogin() throws Exception {

    driver.get(BASE_URI + "#/beer/1");

    DetailPage detailPage = new DetailPage(driver);
    DetailRatingTab ratingTab = detailPage.goToRatingTab();

    ratingTab.setName("user");
    ratingTab.setRating(3);
    ratingTab.setDescription("description");
    ratingTab.clickRateButton();

    LoginModalDialog loginModal = new LoginModalDialog(driver);
    loginModal.logAs("user", "pass");

    Alerts alerts = new Alerts(driver);
    assertThat(alerts.getAlertText()).contains("Vaše hodnocení bylo přidáno, děkujeme");
  }

}
