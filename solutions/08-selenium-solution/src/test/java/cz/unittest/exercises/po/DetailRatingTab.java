package cz.unittest.exercises.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class DetailRatingTab {

  private final WebElement ratings;

  public DetailRatingTab(WebDriver driver) {

    WebDriverWait wait = new WebDriverWait(driver, 10);
    ratings = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.ratings")));
  }

  public void setName(String name) {

    WebElement nameElement = ratings.findElement(By.id("rating-name"));
    nameElement.clear();
    nameElement.sendKeys(name);
  }

  public void setDescription(String text) {

    WebElement nameElement = ratings.findElement(By.id("rating-description"));
    nameElement.clear();
    nameElement.sendKeys(text);
  }

  public void setRating(int rating) {

    List<WebElement> stars = ratings.findElements(By.cssSelector("span#rating-rating>i"));
    stars.get(rating-1).click();
  }

  public void clickRateButton() {

    WebElement button = ratings.findElement(By.cssSelector("button[type='submit']"));
    button.click();
  }

}
