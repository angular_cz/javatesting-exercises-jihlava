package cz.unittest.exercises.availability;

import cz.unittest.exercises.Subject;
import cz.unittest.exercises.User;
import cz.unittest.exercises.stats.StatsRepository;

public class AvailabilityPredictor {

  private StatsRepository statsRepository;

  public AvailabilityPredictor(StatsRepository statsRepository) {
    this.statsRepository = statsRepository;
  }

  public BorrowingInterval getPredictionFor(Subject subject) {
    return statsRepository.getAverageBorrowingTerm(subject);
  }

  public BorrowingInterval getPredictionFor(Subject subject, User user) {

    BorrowingInterval interval = this.getPredictionFor(subject);

    double userCoefficient = statsRepository.getUserCoefficient(user);

    return interval.applyUserCoefficient(userCoefficient);
  }
}
