package cz.unittest.exercises.rules;

import cz.unittest.exercises.Duration;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HalfPriceDurationRuleTest extends AbstractRuleTest {

  @Before
  public void setUp() throws Exception {
    rule = new HalfPriceDurationRule();
  }

  @Test
  public void conditionsForRuleShouldBeApplied() throws Exception {
    assertTrue(rule.canBeAppliedFor(retired, Duration.SIX_MONTHS));
    assertTrue(rule.canBeAppliedFor(child, Duration.SIX_MONTHS));
    assertTrue(rule.canBeAppliedFor(adult, Duration.SIX_MONTHS));
    assertTrue(rule.canBeAppliedFor(young, Duration.SIX_MONTHS));

    assertTrue(rule.canBeAppliedFor(retired, Duration.THREE_MONTHS));
    assertTrue(rule.canBeAppliedFor(child, Duration.THREE_MONTHS));
    assertTrue(rule.canBeAppliedFor(adult, Duration.THREE_MONTHS));
    assertTrue(rule.canBeAppliedFor(young, Duration.THREE_MONTHS));
  }

  @Test
  public void conditionsForRuleShouldNotBeApplied() throws Exception {
    assertFalse(rule.canBeAppliedFor(retired, Duration.ANNUAL));
    assertFalse(rule.canBeAppliedFor(child, Duration.ANNUAL));
    assertFalse(rule.canBeAppliedFor(adult, Duration.ANNUAL));
    assertFalse(rule.canBeAppliedFor(young, Duration.ANNUAL));
  }

  @Test
  public void priceShouldBeHalf() throws Exception {
    assertEquals(50, rule.apply(100));
  }
}
