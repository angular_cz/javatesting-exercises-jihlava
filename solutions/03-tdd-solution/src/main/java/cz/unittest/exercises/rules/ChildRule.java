package cz.unittest.exercises.rules;

import cz.unittest.exercises.Duration;
import cz.unittest.exercises.User;
import cz.unittest.exercises.rules.CalculationRule;
import org.joda.time.LocalDate;

public class ChildRule implements CalculationRule{

  public boolean canBeAppliedFor(User user, Duration duration) {
    int ageInThisMonth = user.getAgeInThisMonth(new LocalDate());
    return ageInThisMonth < 15;
  }

  public int apply(int price) {
    return 0;
  }
}
