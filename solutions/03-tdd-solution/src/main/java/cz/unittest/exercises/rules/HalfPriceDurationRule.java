package cz.unittest.exercises.rules;

import cz.unittest.exercises.Duration;
import cz.unittest.exercises.User;

public class HalfPriceDurationRule implements CalculationRule {

  public boolean canBeAppliedFor(User user, Duration duration) {
    return duration != Duration.ANNUAL;
  }

  public int apply(int price) {
    return price / 2;
  }
}
