package cz.unittest.exercises.rules;

import cz.unittest.exercises.Duration;
import cz.unittest.exercises.User;

public interface CalculationRule {
  boolean canBeAppliedFor(User user, Duration duration);

  int apply(int price);
}
