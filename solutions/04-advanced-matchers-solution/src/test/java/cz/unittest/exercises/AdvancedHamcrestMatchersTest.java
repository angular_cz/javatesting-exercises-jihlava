package cz.unittest.exercises;

import org.hamcrest.*;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AdvancedHamcrestMatchersTest {

  public static final Publication FAVORITE_BOOK = new Publication("ID001CZ", PublicationType.BOOK, "Guide to galaxy", 352);
  public static final List<Publication> SELECTED_BOOKS = Arrays.asList(
    new Publication("ID001CZ", PublicationType.BOOK, "Guide to galaxy", 352),
    new Publication("ID002SK", PublicationType.BOOK, "Goodbye and thank you for fishes", 630),
    new Publication("ID003EN", PublicationType.BOOK, "Clean code", 520)
  );
  public static final List<String> SELECTED_BOOK_IDS = Arrays.asList("ID001CZ", "ID002SK", "ID003CZ");

  @Test
  public void favoriteBookMeetsBasicCriteria() throws Exception {
    assertThat(FAVORITE_BOOK.getType(), equalTo(PublicationType.BOOK));
    assertThat(FAVORITE_BOOK.getPages(), greaterThan(200));

    assertThat(FAVORITE_BOOK.getId(), startsWith("ID"));
    assertThat(FAVORITE_BOOK.getId(), not(endsWith("EN")));
  }

  @Test
  public void allSelectedBookIdsStartsWithID() throws Exception {
    assertThat(SELECTED_BOOK_IDS, everyItem(startsWith("ID")));
  }

  @Test
  public void atLeastOneSelectedBookEndsWithSK() throws Exception {
    assertThat(SELECTED_BOOK_IDS, hasItem(endsWith("SK")));
  }

  @Test
  public void objectPropertyCanBeTestedWithHasPropertyMatcher() throws Exception {
    assertThat(FAVORITE_BOOK, hasProperty("type", equalTo(PublicationType.BOOK)));
  }

  @Test
  public void objectPropertyCanBeTestedWithOwnMatcher() throws Exception {
    assertThat(FAVORITE_BOOK, isType(PublicationType.BOOK));
  }

  private Matcher<Publication> isType(final PublicationType type) {
    return new TypeSafeMatcher<Publication>() {

      public void describeTo(Description description) {
        description.appendText("publication with type")
          .appendValue(type);
      }

      @Override
      protected boolean matchesSafely(Publication publication) {
        return publication.getType().equals(type);
      }
    };
  }

  @Test
  public void selectedBooksMeetsSchoolCriteria() throws Exception {
    assertThat(SELECTED_BOOKS, everyItem(isType(PublicationType.BOOK)));
  }
}
