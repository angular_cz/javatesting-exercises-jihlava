package cz.unittest.exercises;

import cz.unittest.exercises.domain.Borrowing;
import cz.unittest.exercises.domain.GroupedBorrowings;
import cz.unittest.exercises.domain.User;
import cz.unittest.exercises.reminder.ReminderContent;
import cz.unittest.exercises.reminder.ReminderQueue;

import java.util.List;
import java.util.Map;

public class ProcessingJob {

  private BorrowingsRepository borrowingsRepository;
  private ReminderQueue reminderQueue;

  public ProcessingJob(BorrowingsRepository borrowingsRepository, ReminderQueue reminderQueue) {
    this.borrowingsRepository = borrowingsRepository;
    this.reminderQueue = reminderQueue;
  }

  public int process() {

    List<GroupedBorrowings> groupedBorrowings = borrowingsRepository.getGroupedOverdueBorrowings();

    for (GroupedBorrowings borrowings : groupedBorrowings) {
      ReminderContent content = new ReminderContent(borrowings.getBorrowings());
      reminderQueue.add(borrowings.getUser(), content);
    }

    return groupedBorrowings.size();
  }
}
