package cz.unittest.exercises.reminder;

import cz.unittest.exercises.domain.Borrowing;

import java.util.Collections;
import java.util.List;

public class ReminderContent {

  private List<Borrowing> borrowings;

  public ReminderContent(Borrowing borrowing) {
    this.borrowings = Collections.singletonList(borrowing);
  }

  public ReminderContent(List<Borrowing> borrowings) {
    this.borrowings = borrowings;
  }

  public List<Borrowing> getBorrowings() {
    return borrowings;
  }

  public void setBorrowings(List<Borrowing> borrowings) {
    this.borrowings = borrowings;
  }
}
