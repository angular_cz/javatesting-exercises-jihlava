package cz.unittest.exercises.domain;

public class Publication {

  private PublicationType type;
  private String name;

  public Publication(PublicationType type, String name) {
    this.type = type;
    this.name = name;
  }

  public PublicationType getType() {
    return type;
  }

  public void setType(PublicationType type) {
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return type + " " + name;
  }
}
