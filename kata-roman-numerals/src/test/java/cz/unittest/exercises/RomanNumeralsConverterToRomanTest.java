package cz.unittest.exercises;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RomanNumeralsConverterToRomanTest {

  RomanNumeralsConverter converter;

  @Before
  public void instantiateConverter() {
    converter = new RomanNumeralsConverter();
  }

  @Test
  @Ignore("začněte odstraněním tohoto ignore")
  public void convertOne() {
    String result = converter.convertToRoman(1);
    assertEquals("I", result);
  }
}
