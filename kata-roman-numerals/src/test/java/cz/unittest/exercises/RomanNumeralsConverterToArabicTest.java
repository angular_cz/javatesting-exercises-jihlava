package cz.unittest.exercises;

import org.junit.*;

import static org.junit.Assert.*;

public class RomanNumeralsConverterToArabicTest {

  RomanNumeralsConverter converter;

  @Before
  public void instantiateConverter() {
    converter = new RomanNumeralsConverter();
  }

  @Test
  @Ignore("začněte odstraněním tohoto ignore")
  public void convertOner() {
    int result = converter.convertToArabic("I");
    assertEquals(1, result);
  }

}
