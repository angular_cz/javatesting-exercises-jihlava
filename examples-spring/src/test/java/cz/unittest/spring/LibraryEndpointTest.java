package cz.unittest.spring;

import cz.unittest.spring.Book;
import cz.unittest.spring.BookRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql({"classpath:test-db.sql"})
public class LibraryEndpointTest {

  @Autowired
  private TestRestTemplate template;

  @Autowired
  BookRepository bookRepository;

  @Test
  public void thereArePopulatedData() throws Exception {
    Book guide = bookRepository.findOne(1L);
    assertThat(guide.getName()).isEqualTo("Guide to galaxy");
  }

  @Test
  public void itsPossibleToCreateData() throws Exception {

    Book book = new Book();
    book.setName("Created by api");
    book.setAuthor("Author");
    Book createdBook = template.postForObject("/book", book, Book.class);

    Book bookFromApi = template.getForObject( "/book/" + createdBook.getId(), Book.class);
    assertEquals("Created by api", bookFromApi.getName());
  }

  @Test
  public void usingDependenciesIsAllowed() throws Exception {
    Book newBook = new Book();
    newBook.setName("Created by repository");
    newBook.setAuthor("Author");
    newBook.setYear(1985);

    bookRepository.save(newBook);

    Book book = template.getForObject( "/book/" + newBook.getId(), Book.class);
    assertEquals("Created by repository", book.getName());
  }

}
