package cz.unittest.spring;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

public interface BookRepository  extends CrudRepository<Book, Long> {
  Book findOneByName(String name);
}
