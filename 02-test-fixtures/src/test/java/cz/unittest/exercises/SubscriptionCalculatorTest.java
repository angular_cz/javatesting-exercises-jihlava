package cz.unittest.exercises;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

// TODO - 3.4 Přejmenujte třídu na SubscriptionCalculatorForAdultTest
// TODO - 3.5 Abstrahujte obecné vlastnosti testovací třídy
public class SubscriptionCalculatorTest {

  // TODO - 3.1 Vytvořte konstantu pro základní cenu
  // TODO - 3.2 Přesuňte vytvoření uživatele a kalkulátoru do metody
  // TODO - 3.3 Vytvořte metody pro získání ceny

  @Test
  public void calculatePrice_withAdult_shouldReturnBasePrice() {
    User adultUser = new User("Adult user", new LocalDate(1980, 1, 1));
    SubscriptionCalculator calculator = new SubscriptionCalculator(100);

    int annualPrice = calculator.calculatePrice(adultUser, Duration.ANNUAL);

    assertEquals(100, annualPrice);
  }

  @Test
  public void calculatePrice_withAdultForSixMonths_shouldReturnHalfPrice() {
    User adultUser = new User("Adult user", new LocalDate(1980, 1, 1));
    SubscriptionCalculator calculator = new SubscriptionCalculator(100);

    int sixMonthsPrice = calculator.calculatePrice(adultUser, Duration.SIX_MONTHS);

    assertEquals(50, sixMonthsPrice);
  }

}
